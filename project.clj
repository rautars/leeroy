(defproject leeroy "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 [org.clojure/clojure "1.6.0"]
                 [clj-json "0.5.3"]
                 [clj-http "0.9.2"]
                 [clj-time "0.8.0"]
                 [me.raynes/fs "1.4.4"]
                 [org.clojure/tools.logging "0.3.0"]]
  :main ^{:skip-aot true} leeroy.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :repl {:dependencies [[org.clojure/tools.trace "0.7.8"]]}})
