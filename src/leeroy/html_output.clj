(ns leeroy.html-output
  (:gen-class)
  (:require [leeroy.parser :as p]
            [clj-time.format :as f]
            [clj-time.local :as l]
            [clj-time.coerce :as c]
            [clojure.string :as string]
            [clojure.tools.logging :as log])
  (:use [clojure.walk :as walk]))

(def style
  (str "<style>"
       ".success {background-color:#c6efce; color:#006100}"
       ".unstable {background-color:#ffeb9c; color:#9c6500}"
       ".failure {background-color:#ffc7ce; color:#9c0006}"
       ".unknown {background-color:#949494; color:#ffffff}"
       "a {color: rgb(9, 69, 92)}"
       "</style>"))

(defn link
  [text url]
  (str "<a href=\"" url "\">" text "</a>"))

(def header
  (str "<header>"
       style
       "</header>"))

(def body-header
  (let [report-name-formatter (f/formatter-local "yyyy-MM-dd hh")
        date-and-hours (f/unparse report-name-formatter (l/local-now))]
    (str "<big><b>" p/report-name " " date-and-hours "h"
         "</b></big><br/><small>"
         "<b>Jenkins:</b> " (link p/jenkins-url p/jenkins-url)
         "</b></small><br/><br/>"
         )))

(def table-header
  "Table columns names"
  (str "<table style=\"border: 1px solid rgb(99, 99, 99); border-collapse:collapse; padding: 3px;\">"
       "<tbody style=\"border: inherit\">"
       "<tr style=\"border: inherit\">"
       "<th style=\"border: inherit\">Component</th>"
       "<th style=\"border: inherit\">Status</th>"
       "<th style=\"border: inherit\">Age</th>"
       "<th style=\"border: inherit\">Test Result</th>"
       "<th style=\"border: inherit\">Comment</th>"
       "</tr>"))

(defn table-row
  "Generate table row from provided job data"
  [job-result]
  (str "<tr style=\"border: inherit\">"
       "<td style=\"border: inherit\">" (get job-result :name) "</td>"
       "<td style=\"border: inherit\" class=\"" (get job-result :status-class) "\">" (get job-result :status) "</td>"
       "<td style=\"border: inherit\">" (get job-result :age) "</td>"
       "<td style=\"border: inherit\">" (get job-result :report) "</td>"
       "<td style=\"border: inherit\">" (get job-result :comment) "</td>"
       "</tr>"))

(def table-footer
  (str "<tbody>"
       "</table>"))

(defn table-content
  "Generate table rows (<tr>) from job result data"
  [job-results]
  (walk #(table-row %) #(apply str %) job-results))

(defn styled-status
  [status-class]
  (if (= status-class "unknown") "N/A"
    (string/capitalize status-class)))

(defn styled-fail
  [failures-count url]
  (let [test-report-url (str url "testReport/")]
    (if (nil? failures-count)
      "N/A"
      (if (> failures-count 0)
        (if (= failures-count 1)
          (link "1 failure" test-report-url)
          (link (str failures-count " failures") test-report-url))
        "no failures"))))

(defn styled-skip
  [skip-count]
  (if (nil? skip-count)
    nil
    (if (> skip-count 0)
      (str "(" skip-count " skipped)"))))

(defn styled-age
  [timestamp]
  (if (nil? timestamp) "N/A"
    (let [current-timestamp (c/to-long (l/local-now))
          age (float (/ (- current-timestamp timestamp) 1000 60 60 24))
          formated-age (str (format "%.1f" age) "d")]
      (if (>= age 1.0)
        (str "<b>" formated-age "</b>")
        formated-age))))

(defn styled-result
  [result url]
  (str (styled-fail (get result :fail) url)
       " "
       (styled-skip (get result :skip))))

(defn analysis-priority-link
  [base-url priority]
  (cond
   (= priority "high") (link "High Priority" (str base-url "analysisResult/HIGH"))
   (= priority "normal") (link "Normal Priority" (str base-url "analysisResult/NORMAL"))
   (= priority "low") (link "Low Priority" (str base-url "analysisResult/LOW"))))

(defn styled-workflow-link
  [build-result]
  (let [build-url (p/build-url build-result)
        workflow-url (p/find-build-workflow-url build-result)]
    (str "see " (link  "workflow" workflow-url))))

(defn styled-console-link
  [build-result]
  (let [build-url (p/build-url build-result)]
    (str "see " (link "console" (str build-url "console")))))

(defn styled-analysis
  [build-results]
  (let [base-url (p/build-url build-results)
        analysis-data (p/analysis-data build-results)]
    (if (nil? analysis-data)
      "N/A"
      (let [number-of-high (get analysis-data :high)
            number-of-normal (get analysis-data :normal)
            number-of-low (get analysis-data :low)]
        ;; Analysis result construction
        (str
          (if (> number-of-high 0)
            (str number-of-high " warnings of " (analysis-priority-link base-url "high")))
          (if (> number-of-normal 0)
            (do
              (if (> number-of-high 0) "<br/>")
              (str number-of-normal " warnings of " (analysis-priority-link base-url "normal"))))
          (if (> number-of-low 0)
            (do
              (if (> number-of-normal 0) "<br/>")
              (str number-of-low " warnings of " (analysis-priority-link base-url "low"))))
          )))))

(defn construct-comment
  [job-data build-results]
  (let [status (p/job-result build-results)
        analysis-data? (p/analysis-result? job-data)
        comment-type (p/comment-type job-data)]
    (str
      (if analysis-data?
        (styled-analysis build-results)
        (if (= status p/failure)
          (case comment-type
            "linkToWorkFlow" (styled-workflow-link build-results)
            "comment" (styled-console-link build-results)
            (styled-console-link build-results)
            )
          )
        )
      )
    ))



(defn output
  "Build full html output"
  [job-results]
  (str "<html>"
       header
       "<body>"
       body-header
       table-header
       (table-content job-results)
       table-footer
       "</body>"
       "</html>"))
