(ns leeroy.parser
  (:gen-class)
  (:require [clj-json [core :as json]]
            [clojure.tools.logging :as log])
  (:use [leeroy.utils :as u]
        [clojure.walk :as walk]))

(defn load-config
  "load config content from json file and set constant variables"
  [config-file]
  (try
    (let [config-content (json/parse-string (slurp config-file))]
      (def app-config (get config-content "app"))
      (def report-name (get app-config "reportName"))
      (def jenkins-url (get app-config "jenkinsUrl"))
      (def max-attempts (get app-config "maxAttempts"))
      (def jobs (get config-content "jobs")))
    (catch java.io.FileNotFoundException e
      (log/error (str "Config file \"" config-file "\" was not found"))
      (java.lang.System/exit 0))))

;; Constants
(def failure "FAILURE")

(def success "SUCCESS")

(def unstable "UNSTABLE")

(defn analysis-result?
  "checks if analysis flag is set for job"
  [job]
  (let [analysis (get job "analysisResult")]
    (if (nil? analysis) false
                        (if (= analysis true) true false))))

(defn api-url
  "return full url to json object"
  [base-url build-number]
  (let [url-end "api/json"]
    (if
      (nil? build-number) (apply str [base-url url-end])
      (str base-url build-number "/" url-end))))

(defn analysis-result-url
  "return url to analysis result json object"
  [job-url]
  (str job-url "analysisResult/api/json"))


;; *** JSON object parsing ***
(defn job-filter
  "return build filter"
  [job]
  (get job "filter"))

(defn comment-type
  [job-data]
  (get job-data "onFailure"))

(defn job-name
  "return job name"
  [job]
  (get job "name"))

(defn build-timestamp
  "return build timestamp"
  [job]
  (get job "timestamp"))

(defn build-url
  "return build url"
  [build-results]
  (get build-results "url"))

(defn job-result
  "return build status"
  [job]
  (get job "result"))

(defn build-body-content
  "return body from json object"
  [api-url]
  (let [page-content (u/get-page api-url)]
    (if (u/not-nil? page-content)
      (json/parse-string
       (get page-content :body)))))

(defn build-desc
  "return build description"
  [build-body-content]
  (get build-body-content "description"))

(defn correct-desc?
  "checks if build description contains specified text"
  [build-body-content desc-filter]
  (u/substring? desc-filter (build-desc build-body-content)))

(defn correct-build?
  "check if build meets filter criteria"
  [base-url filter build-nr]
  (if (nil? filter)
    true
    (let [content (build-body-content (api-url base-url build-nr))]
      (if (nil? content) false
        (correct-desc? content filter)))))

(defn analysis-data
  ""
  [build-data]
  (let [build-url (build-url build-data)
        analysis-result-url (analysis-result-url build-url)
        result (build-body-content analysis-result-url)]
    (if (nil? result) nil
                      {:low (get result "numberOfLowPriorityWarnings")
                       :normal (get result "numberOfNormalPriorityWarnings")
                       :high (get result "numberOfHighPriorityWarnings")})))


(defn last-completed-build
  "return last completed build from build data"
  [build-data]
  (get build-data "lastCompletedBuild"))

(defn build-number
  "return build number"
  [build]
  (get build "number"))

(defn job-actions
  "return actions list"
  [job]
  (get job "actions"))

(defn build-workflow-url
  ""
  [job-action]
  (if (= "BUILD_FLOW_URL" (get job-action "name"))
    (get job-action "value")))

(defn find-build-workflow-url
  ""
  [build-result]
  (let [job-actions (job-actions build-result)
        first-action (first job-actions)
        action-parameters (get first-action "parameters")]
    (walk
      #(if (u/not-nil? (build-workflow-url %))
        (build-workflow-url %))
      #(first
        (filter (fn [x] (u/not-nil? x)) %))
      action-parameters)))