(ns leeroy.core
  (:gen-class)
  (:require [clj-json [core :as json]]
            [clj-http.client :as client]
            [clj-time.core :as t]
            [clj-time.local :as l]
            [clj-time.format :as f]
            [clojure.string :as string]
            [clojure.tools.logging :as log])
  (:use [clojure.walk :as walk]
        [leeroy.utils :as u]
        [leeroy.parser :as p]
        [leeroy.html-output :as html]))

(def json-api "api/json")

(defn filter-build-nr
  "Filters out numbers based on filter keyword"
  [base-url desc-filter build-nr remained-builds]
  (cond
   (nil? build-nr) nil
   (p/correct-build? base-url desc-filter build-nr) build-nr
   (> remained-builds 0) (filter-build-nr base-url desc-filter (- build-nr 1) (- remained-builds 1))
   :else nil))

(defn top-build-nr
  "return top last completed build number"
  [job-url]
  (build-number
   (p/last-completed-build
    (build-body-content
     (api-url job-url nil)))))

(defn test-report?
  ""
  [job-action]
  (if (contains? job-action "urlName")
    (if (= "testReport" (get job-action "urlName")) true false) false))

(defn test-report
  ""
  [job-actions]
  (walk
   #(if (test-report? %)
      {:fail (get % "failCount") :skip (get % "skipCount")})
   #(first
     (filter (fn [x] (u/not-nil? x)) %))
   job-actions))



(defn status-class
  [status]
  (cond
   (= status "SUCCESS") "success"
   (= status "UNSTABLE") "unstable"
   (= status "FAILURE") "failure"
   :else "unknown"))

(defn get-build-content
  [build-data]
  (let [build-url (p/build-url build-data)
        desc-filter (job-filter build-data)
        top-build-nr (top-build-nr build-url)
        build-number (filter-build-nr build-url desc-filter top-build-nr max-attempts)]
    (p/build-body-content
      (p/api-url build-url build-number))
  ))

(defn necessary-info
  ""
  [build-data]
  (let [build-result (get-build-content build-data)
        url (build-url build-result)
        status-class (status-class (job-result build-result))
        job-name (job-name build-data)]
    (log/info (str "Gathering info from job: " job-name " ..."))
    {:url url
     :name (link job-name url)
     :status (styled-status status-class)
     :status-class status-class
     :age (styled-age (build-timestamp build-result))
     :report (styled-result (test-report (job-actions build-result)) url)
     :comment (html/construct-comment build-data build-result)
     }))

(defn job-results
  ""
  []
  (walk #(necessary-info %) #(doall %) p/jobs))

(defn -main [& args]
  (p/load-config "config.json")

  (println "Starting gathering report")
  (u/save-report (html/output (job-results)))
  (println "DONE"))

