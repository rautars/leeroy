(ns leeroy.html-output-test
  (:require [clojure.test :refer :all]
            [leeroy.html-output :refer :all]))

(deftest styled-analysis-test
  (testing "styled-analysis method"
    (with-redefs-fn {#'leeroy.html-output/analysis-priority-link (fn [s ss] nil)}
                    (fn []
                      (is (= "1 warnings of " (styled-analysis "s" {:low 1 :normal 0 :high 0} )))
                      (is (= "1 warnings of " (styled-analysis "s" {:low 0 :normal 1 :high 0} )))
                      (is (= "1 warnings of " (styled-analysis "s" {:low 0 :normal 0 :high 1} )))
                      (is (= "3 warnings of 2 warnings of 1 warnings of " (styled-analysis "s" {:low 1 :normal 2 :high 3} )))))))

(deftest styled-workflow-link-test
  (testing "styled workflow link method"
    (is (= "see <a href=\"url/workflow\">workflow</a>" (styled-workflow-link "url/")))))

(deftest styled-console-link-test
  (testing "styled console link method"
    (is (= "see <a href=\"url/console\">console</a>" (styled-console-link "url/")))))
