(ns leeroy.parser-test
  (:require [clojure.test :refer :all]
            [leeroy.parser :refer :all]))

;; Commented out - slurp method mocking does not working :(

;; (deftest load-config-test
;;   (testing "load-config method"
;;     (with-redefs-fn {#'clojure.core/slurp (fn [s]
;;                      "{\"app\":{
;;                      \"buildMaxAge\" : 24,
;;                      \"maxAttempts\" : 30,
;;                      \"jenkinsUrl\" : \"fake-url\",
;;                      \"reportName\" : \"fake-name\"},
;;                      \"jobs\": [\"job1\" : {}]\"}")}
;;       (do
;;         (load-config "s")
;;         (is (= "fake-name" report-name))))))

(deftest api-url-test
  (testing "api-url method"
    (is (= "base-url/1/api/json") (api-url "base-url" 1))))

(deftest analysis-result-url-test
  (testing "analysis-result-url method"
    (is (= "base-url/analysisResult/api/json" (analysis-result-url "base-url/")))))

(deftest build-body-content-test
  (testing "json parsing"
    (with-redefs-fn {#'clj-http.client/get (fn [s] {:body "{\"content\": 0}"})}
      #(is (= {"content" 0}) (build-body-content "s")))))

(deftest correct-build?-test
  (testing "build filtering"
    (is (= true (correct-build? "s" nil 0)))
    (with-redefs-fn {#'leeroy.parser/build-body-content (fn [s] nil)}
      (fn []
        (with-redefs-fn {#'leeroy.parser/correct-desc? (fn [s ss] false)}
          #(is (= false (correct-build? "s" "ss" 0))))
        (with-redefs-fn {#'leeroy.parser/correct-desc? (fn [s ss] true)}
          #(is (= false (correct-build? "s" "ss" 0))))))
    (with-redefs-fn {#'leeroy.parser/build-body-content (fn [s] "not-nill")}
      (fn []
        (with-redefs-fn {#'leeroy.parser/correct-desc? (fn [s ss] false)}
          #(is (= false (correct-build? "s" "ss" 0))))
        (with-redefs-fn {#'leeroy.parser/correct-desc? (fn [s ss] true)}
          #(is (= true (correct-build? "s" "ss" 0))))))))
