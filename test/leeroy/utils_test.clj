(ns leeroy.utils-test
  (:require [clojure.test :refer :all]
            [leeroy.utils :refer :all]))

(deftest not-nil?-test
	(testing "not-nil? check"
		(is (= false (not-nil? nil)))
    (is (= true (not-nil? "not-nil")))
    (is (= true (not-nil? 0)))
    (is (= true (not-nil? false)))))

(deftest substring?-test
	(testing "substring? check"
		(is (thrown? NullPointerException (substring? nil "s")))
    (is (thrown? NullPointerException (substring? "s" nil)))
    (is (= false (substring? "tests" "0test$")))
    (is (= true (substring? "test" "0test$"))))
    (is (= true (substring? "s" "s"))))

(deftest get-page-test
  (testing "get page method"
    (is (nil? (get-page "fake-url")))
    (with-redefs-fn {#'clj-http.client/get (fn [s] "content")}
      #(is (= "content" (get-page "s"))))))

(deftest save-report-test
  (testing "save report method"
    (with-redefs-fn {#'clojure.core/spit (fn [name content] {:name name :content content})}
      #(let [content "fake-content"
             result (save-report content)]
         (is (not-nil? (get result :name)))
         (is (= content (get result :content)))))))
